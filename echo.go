package main

import (
			"os"
			"github.com/bwmarrin/discordgo"
	"encoding/json"
	"log"
	"strings"
)

var l *log.Logger

func init() {
	err := os.Chdir("modules/echo")
	if err != nil {
		panic(err)
	}
	file, err := os.OpenFile("moduleEcho.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		panic(err)
	}
	l = log.New(file, "", log.Ldate|log.Ltime|log.Llongfile)
}

func main() {
	l.Println("Command is echo")
	args := os.Args[1:]
	token := args[0]
	message := args[1]

	var m discordgo.Message
	err := json.Unmarshal([]byte(message), &m)
	if err != nil {
		l.Println(err)
		return
	}

	// Create a new Discord session using bot token.
	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		l.Println("Error creating Discord session:", err)
		return
	}
	l.Println("Discord session created")

	// Open the web socket and begin listening.
	err = dg.Open()
	if err != nil {
		l.Println("Error opening Discord session:", err)
	}
	l.Println("New session opened")

	// Close down after completion
	defer func() {
		dg.Close()
		l.Println("New session closed")
	}()

	l.Println(m.Content)
	response := strings.Join(strings.Split(m.Content, " ")[1:], " ") // Strip out the command
	if response == "" {
		response = "Nothing to echo!"
	}
	dg.ChannelMessageSend(m.ChannelID, response)
}
